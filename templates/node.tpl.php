<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

	<?php print $user_picture; ?>

	<?php print render($title_prefix); ?>
	<?php if (!$page): ?>
		<h2<?php print $title_attributes; ?>><?php print $title_additional; ?><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
	<?php endif; ?>
	<?php print render($title_suffix); ?>

	<?php if ($display_submitted): ?>
		<span class="submitted"><?php print $submitted ?></span>
	<?php endif; ?>

	<div class="content clearfix"<?php print $content_attributes; ?>>	
		<?php
      hide($content['taxonomy_vocabulary_2']);
      hide($content['field_tags']);
			hide($content['comments']);
      hide($content['comments']['comment_form']['comment_body']['und'][0]['format']);
			hide($content['links']);
			print render($content);
		?>
    
    <?php if ($node_bottom): ?>
      <div class = "node-bottom">
        <?php print render($node_bottom); ?>
      </div>
    <?php endif; ?>
    
	</div>

  <div class = "bottom clearfix">

    <?php if (isset($bottom_links) && $bottom_links): ?>
      <div class = "bottom-links">
        <?php print $bottom_links; ?>
      </div>
    <?php endif; ?>

    <?php if (isset($bottom_right_links) && $bottom_right_links): ?>
      <div class = "bottom-right-links">
        <?php print $bottom_right_links; ?>
      </div>
    <?php endif; ?>

  </div>

</div>

<?php if ($page): ?>
  <div class = "social-links clearfix">
    Друзья тоже любят читать интересные статьи
    <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
      <a class="addthis_button_facebook"></a>
      <a class="addthis_button_twitter"></a>
      <a class="addthis_button_vk"></a>
      <a class="addthis_button_odnoklassniki_ru"></a>
      <a class="addthis_button_mymailru"></a>
      <a class="addthis_button_livejournal"></a>
      <a class="addthis_button_formspring"></a>
      <a class="addthis_button_google"></a>
    </div>
  </div>
<?php endif; ?>

<?php if (isset($navigation) && $navigation): ?>
  <div class = "node-navigation clearfix">

    <?php if ($previous_node): ?>
      <div class = "node-prev"><?php print $previous_node; ?></div>
    <?php endif; ?>

    <?php if ($next_node): ?>
      <div class = "node-next"><?php print $next_node; ?></div>
    <?php endif; ?>

    <div class = "nav">Навигация</div>

  </div>
<?php endif; ?>

<?php if (isset($similar)): ?>
  <div class = "node-similar">
    <?php print render($similar); ?>
  </div>
<?php endif; ?>

<div class="clearfix">
	<?php print render($content['comments']); ?>  
</div>