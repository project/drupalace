<?php

/**
 * Override theme_button().
 */
function drupalace_button($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array('id', 'name', 'value'));

  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled';
  }

  return '<span class="button"><span class = "button-left"></span><span class = "button-center"></span><span class = "button-right"></span><input' . drupal_attributes($element['#attributes']) . ' /></span>';
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function drupalace_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  if (!empty($breadcrumb)) {
    $output = '<div class="breadcrumb">' . implode(' › ', $breadcrumb) . '</div>';
    return $output;
  }
}

/**
 * Override or insert variables into the html template.
 */
function drupalace_preprocess_html(&$vars) {
  // Add conditional CSS for IE7 and lower.
  drupal_add_css(path_to_theme() . '/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
}

/**
 * Override or insert variables into the page template.
 */
function drupalace_preprocess_page(&$vars) {
  global $user;
  // Move secondary tabs into a separate variable.
  $vars['tabs2'] = array(
    '#theme' => 'menu_local_tasks',
    '#secondary' => $vars['tabs']['#secondary'],
  );
  unset($vars['tabs']['#secondary']);

  if (isset($vars['node'])) {
    $node = $vars['node'];
    $type = node_type_get_name($node);
    $url = $node->type;
    if ($node->type == 'lesson') {
      $url = 'uroki';
    }
    elseif ($node->type == 'question') {
      $url = 'questions';
    }

    $vars['title_additional'] = l($type, $url, array('attributes' => array('class' => array('additional')))) . '<span class = "arrow"></span>';

    // Add title class
    $vars['title_attributes_array']['class'][] = 'title';
    $vars['title_attributes_array']['class'][] = $node->type;
    $vars['theme_hook_suggestions'][] = 'page__node__full';
  }

  if (!$user->uid && arg(0) != 'user') {
    drupal_add_library('system', 'ui.dialog');
    drupal_add_library('system', 'effects.explode');
    drupal_add_library('system', 'effects.slide');
    $vars['login_button'] = l('Вход', 'user', array('attributes' => array('class' => array('user-login'))));
  }

}

/**
 * Override or insert variables into the node template.
 */
function drupalace_preprocess_node(&$vars) {
  // Add additional link in title
  $node = $vars['node'];
  $type = node_type_get_name($node);
  $url = $node->type;
  if ($node->type == 'lesson') {
    $url = 'uroki';
  }
  elseif ($node->type == 'question') {
    $url = 'questions';
  }
  elseif ($node->type == 'blog2') {
    $url = 'blog';
  }

  $vars['title_additional'] = l($type, $url, array('attributes' => array('class' => array('additional')))) . '<span class = "arrow"></span>';

  // Add title class
  $vars['title_attributes_array']['class'][] = 'title';
  $vars['title_attributes_array']['class'][] = $node->type;

  // Add node bottom links
  $bottom_links = array();
  $bottom_links[] = '<span class = "name">' . $vars['name'] . '</span>';
  $bottom_links[] = '<span class = "date">' . date('d.m.Y', $node->created) . '</span>';
  if (module_exists('statistics')) {
    if(user_access('view post access counter')) {
      $statistics = statistics_get($node->nid);
      $bottom_links[] = '<span class = "view">' . $statistics['totalcount'] . '</span>';
    }
  }
  $vars['bottom_links'] = theme('item_list', array('items' => $bottom_links));

  // Add node bottom additional links
  $links = array();
  if ($vars['teaser']) {
    $links[] = '<span class = "comments">' . l(format_plural($node->comment_count, '1 comment', '@count comments'), "node/$node->nid", array('attributes' => array('title' => $node->title), 'fragment' => 'comments', 'html' => TRUE)) . '</span>';
    $links[] = '<span class = "readmore">' . l(t('Read more'), "node/$node->nid", array('attributes' => array('title' => $node->title))) . '</span>';
    $bottom_links = theme('item_list', array('items' => $links));
  }
  else {
    if (isset($vars['node']->taxonomy_vocabulary_2['und']) && count($vars['node']->taxonomy_vocabulary_2['und']) > 0) {
      foreach ($vars['node']->taxonomy_vocabulary_2['und'] as $term) {
        $links[] = l($term['taxonomy_term']->name, 'taxonomy/term/' . $term['tid']);
      }
      $bottom_links = '<div class = "tags">' . theme('item_list', array('items' => $links)) . '</div>';
    }
    elseif (isset($vars['node']->field_tags['und']) && count($vars['node']->field_tags['und']) > 0) {
      foreach ($vars['node']->field_tags['und'] as $term) {
        $links[] = l($term['taxonomy_term']->name, 'taxonomy/term/' . $term['tid']);
      }
      $bottom_links = '<div class = "tags">' . theme('item_list', array('items' => $links)) . '</div>';
    }
  }

  if ($links) {
    $vars['bottom_right_links'] = $bottom_links;
  }

  // Add navigation
  if ($vars['page'] && in_array($node->type, array('lesson', 'design', 'question', 'blog'))) {
    $vars['previous_node'] = previous_node($node);
    $vars['next_node']     = next_node($node);
    $vars['navigation']    = TRUE;
  }

  // Add region to node
  $vars['node_bottom'] = block_get_blocks_by_region('node_bottom');

  // Add similar entries
  if ($vars['page'] && $vars['node']) {
    $tids = array();
    if (isset($vars['taxonomy_vocabulary_2']) && count($vars['taxonomy_vocabulary_2']) > 0) {    
      foreach ($vars['taxonomy_vocabulary_2'] as $tag) {
        $tids[] = $tag['tid'];
      }
    }
    elseif (isset($vars['field_tags']) && count($vars['field_tags']) > 0) {    
      foreach ($vars['field_tags'] as $tag) {
        if (isset($tag['tid'])) {
          $tids[] = $tag['tid'];
        }
      }
    }
    if ($tids) {
      $query = db_select('node', 'n');
      $query->fields('n', array('nid', 'title'));
      $query->addExpression('COUNT(*)', 'hits');
      $query->leftJoin('taxonomy_index', 'ti', 'n.nid = ti.nid');
      $query->condition('n.type', $vars['type']);
      $query->condition('n.status', NODE_PUBLISHED);
      $query->condition('ti.tid', $tids, 'IN');
      $query->condition('n.nid', $vars['nid'], '<>');
      $query->groupBy('n.nid');
      $query->orderBy('hits', 'DESC');
      $query->orderBy('n.created', 'DESC');
      $query->range(0, 3);
      $vars['similar'] = node_title_list($query->execute(), 'Похожие материалы');
    }
  }
}

function previous_node($node) {
  $previous_node = db_query("SELECT nid, title FROM {node} WHERE created < :created AND status = 1 AND type = :type ORDER BY created DESC LIMIT 0, 1",
    array(':created' => $node->created, ':type' => $node->type))->fetchObject();
  if ($previous_node) {
    return l($previous_node->title, 'node/' .  $previous_node->nid);
  }
  return '';
}

function next_node($node) {
  $next_node = db_query("SELECT nid, title FROM {node} WHERE created > :created AND status = 1 AND type = :type ORDER BY created ASC LIMIT 0, 1",
    array(':created' => $node->created, ':type' => $node->type))->fetchObject();
  if ($next_node) {
    return l($next_node->title, 'node/' .  $next_node->nid);
  }
  return '';
}

/**
 * Override or insert variables into the comment template.
 */
function drupalace_preprocess_comment(&$vars) {
  $vars['created'] = date('d.m.Y H:i', $vars['comment']->created);
  $vars['author'] = t('!author wrote:', array('!author' => $vars['author']));
  $uri = entity_uri('comment', $vars['comment']);
  $uri['options'] += array('attributes' => array(
    'class' => 'permalink',
    'rel' => 'bookmark',
  ));
  $vars['permalink'] = l('#' . $vars['id'], $uri['path'], $uri['options']);
}

/**
 * Override or insert variables into the block template.
 */
function drupalace_preprocess_block(&$vars) {
  $vars['title_attributes_array']['class'][] = 'title';
}

/**
 * Override or insert variables into the region template.
 */
function drupalace_preprocess_region(&$vars) {
  if ($vars['region'] == 'footer') {

  }
}

/**
 * Override default theme_filter_tips().
 */
function drupalace_filter_tips($variables) {
  $tips = $variables['tips'];
  $output = '';

  $multiple = count($tips) > 1;
  if ($multiple) {
    $output = '<h2>' . t('Text Formats') . '</h2>';
  }

  if (count($tips)) {
    if ($multiple) {
      $output .= '<div class="compose-tips">';
    }
    foreach ($tips as $name => $tiplist) {
      if ($multiple) {
        $output .= '<div class="filter-type filter-' . drupal_html_class($name) . '">';
        $output .= '<h3>' . $name . '</h3>';
      }

      if ($multiple) {
        $output .= '</div>';
      }
    }
    if ($multiple) {
      $output .= '</div>';
    }
  }

  return $output;
}

/**
 * Override default theme_filter_tips_more_info().
 */
function drupalace_filter_tips_more_info() {
  return '';
}

/**
 * Implements hook_html_head_alter().
 */
function drupalace_html_head_alter(&$head_elements) {
  // Unset shortlinks
  foreach ($head_elements as $key => $element) {
    if (isset($element['#attributes']['rel']) && $element['#attributes']['rel'] == 'shortlink') {
      unset($head_elements[$key]);
    }
  }
}

/**
 * Overrides theme_links().
 */
function drupalace_links($variables) {
  $links = $variables['links'];
  $attributes = $variables['attributes'];
  $heading = $variables['heading'];
  global $language_url;
  $output = '';

  if (count($links) > 0) {
    $output = '';

    // Treat the heading first if it is present to prepend it to the
    // list of links.
    if (!empty($heading)) {
      if (is_string($heading)) {
        // Prepare the array that will be used when the passed heading
        // is a string.
        $heading = array(
          'text' => $heading,
          // Set the default level of the heading.
          'level' => 'h2',
        );
      }
      $output .= '<' . $heading['level'];
      if (!empty($heading['class'])) {
        $output .= drupal_attributes(array('class' => $heading['class']));
      }
      $output .= '>' . check_plain($heading['text']) . '</' . $heading['level'] . '>';
    }

    $output .= '<ul' . drupal_attributes($attributes) . '>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = array($key);

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class[] = 'first';
      }
      if ($i == $num_links) {
        $class[] = 'last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
           && (empty($link['language']) || $link['language']->language == $language_url->language)) {
        $class[] = 'active';
      }
      $output .= '<li' . drupal_attributes(array('class' => $class)) . '>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= '<span class = "menu-left"></span><span class = "menu-center"></span><span class = "menu-right"></span>' . l($link['title'], $link['href'], $link);
      }
      elseif (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes.
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span' . $span_attributes . '>' . $link['title'] . '</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function drupalace_form_search_block_form_alter(&$form, &$form_state, $form_id) {
  $value = t('Drupal');
  $form['search_block_form']['#default_value'] = $value;
  $form['search_block_form']['#attributes'] = array(
    'onblur'=> "if (this.value.length == 0) {this.value='{$value}'}",
    'onfocus'=> "if (this.value== '{$value}') {this.value=''}"
  );
  $form['actions']['submit']['#suffix'] = '<span class="spic"></span>';
  unset($form['search_block_form']['title']);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function drupalace_form_comment_form_alter(&$form, &$form_state, $form_id) {
  global $user;
  $lang = $form['comment_body']['#language'];
  $form['comment_body'][$lang][0]['#resizable'] = FALSE;
  $form['#attributes']['class'][] = 'clearfix';
  if (!$user->uid) {
    $form['#attributes']['class'][] = 'anonymous';
  }
}

/**
 * Override theme_pager().
 */
function drupalace_pager($variables) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : ''), 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : ''), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : ''), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : ''), 'element' => $element, 'parameters' => $parameters));

  $items = array();
  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => array('pager-first'),
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => array('pager-previous'),
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('pager-current'),
            'data' => $i,
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => array('pager-next'),
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'class' => array('pager-last'),
        'data' => $li_last,
      );
    }
    return '<h2 class="element-invisible">' . t('Pages') . '</h2>' . theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('pager')),
    ));
  }
}

/**
 * Override theme_item_list().
 */
function drupalace_item_list($variables) {
  $items = $variables['items'];
  $title = $variables['title'];
  $type = $variables['type'];
  $attributes = $variables['attributes'];

  $output = '<div class="item-list">';
  if (isset($title)) {
    $output .= '<h3>' . $title . '</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type" . drupal_attributes($attributes) . '>';
    $num_items = count($items);
    foreach ($items as $i => $item) {
      $attributes = array();
      $children = array();
      $data = '';
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        // Render nested list.
        $data .= theme_item_list(array('items' => $children, 'title' => NULL, 'type' => $type, 'attributes' => $attributes));
      }
      if ($i == 0) {
        $attributes['class'][] = 'first';
      }
      if ($i == $num_items - 1) {
        $attributes['class'][] = 'last';
      }
      if ($i % 2 == 0) {
        $attributes['class'][] = 'odd';
      }
      else {
        $attributes['class'][] = 'even';
      }
      $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
    }
    $output .= "</$type>";
  }
  $output .= '</div>';
  return $output;
}