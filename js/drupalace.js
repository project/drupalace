(function($) {

$(document).ready(function() {
  var login_block = $('#block-user-login');
  if (login_block.length > 0) {
    login_block.dialog({
      autoOpen: false,
      title: 'Вход на сайт',
      resizable: false,
      maxWidth: 245,
      modal: true,
      show: "slide",
			hide: "explode"
    });
    $('.user-login').click(function() {
      login_block.dialog('open');
      return false;
    });
  }
});

})(jQuery);